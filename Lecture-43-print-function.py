#print function default functionality

# Note:- It will  create blank line next line

print("Welcome to india",end='\n')

#Ignore next line blank 
print("Welcome to india",end=' ')

# To separate print word
print("AWS","Azure","Gcp","python","Ansible",sep='\n')


#sep and end operator use to gather.

print("Aws","Azure","Linux","Ansible","Python",sep='\n',end='\nwelcome to india')

#Break line

print("Welcome \nto india")

#print function with string value

print("Ram" + "Panwar")

# To get space between name
print("Ram" + " Panwar")

#We can also use like bellow
print("Ram","Panwar")

#print int and str to gather

#print(10 + "test")

#Note:- It will not work to gather. * (Multiply) operator will work.

print(10 * "welcome india",end='\n')

#It will print test word 10 times

#need space beween the words

print(10 * "welcome india ",end='\n')

#check variable data type. It will not work. (Wrong)
a=10
print(a)
type(a)

#Right

a=10
print(a)
print(type(a))

