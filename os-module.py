"""
# import os module
import os

# get current directory.
print(os.getcwd())

# create directory using python
os.mkdir("python-tutorials")

#Delete directory
os.rmdir("python-tutorials")

#rename directory

os.rename("python-tutorials", "python-tutorials-new")

#-------------> Basic file manipulation <----------------#

#Create file 

f = open("python.txt", "w")

#Write contenet in file

f.write("Woops! I have deleted the content!")

f.close()

# Read the file content after append 

f = open("python.txt","r")
print(f.read())

"""

#Create  director

import os

if os.path.exists("ram"):
    os.rmdir("ram")
    print("directory deleted now")
else:
    os.mkdir("ram")
    print("Directory created now")

    
        