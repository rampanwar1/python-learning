import boto3
import os
from dotenv import load_dotenv
load_dotenv()

def scale_down_eks_node_group(eks_cluster_name, eks_nodegroup_name, desired_capacity, mini_size, max_size):

    
    session = boto3.Session(region_name=aws_region)
    eks_client = session.client('eks')

    # Get the current node group configuration
    response = eks_client.describe_nodegroup(
        clusterName=eks_cluster_name,
        nodegroupName=eks_nodegroup_name
    )

    # Update the node group with the new desired capacity
    response = eks_client.update_nodegroup_config(
        clusterName=eks_cluster_name,
        nodegroupName=eks_nodegroup_name,
        scalingConfig={
            'desiredSize': desired_capacity,
            'minSize': mini_size,
            'maxSize': max_size
        }
    )

    print(f"\n ||======> Scaling down {eks_nodegroup_name} in {eks_cluster_name} to max instances {max_size} and minimum Instances {mini_size}. <========||")

if __name__ == "__main__":
    eks_cluster_name = os.getenv("CLUSTER_NAME")
    eks_nodegroup_name = os.getenv("NODE_GROUP_NAME")
    aws_region = os.getenv("REGION_NAME")
    desired_capacity = int(os.getenv("SCALE_DOWN_INSTANCES_COUNT"))
    mini_size = int(os.getenv("SCALE_DOWN_INSTANCES_COUNT"))
    max_size = int(os.getenv("SCALE_DOWN_MAX_SIZE"))
    
    scale_down_eks_node_group(eks_cluster_name, eks_nodegroup_name, desired_capacity, mini_size, max_size)
