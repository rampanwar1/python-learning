import os 

#------------------> create multiple directory inside the directory <---------------------#

"""

for i in range(0, 100):
    os.mkdir(f"data/Day{i +1}")
    
"""



#-------------> Change dirs name above created <---------------------------#

"""
for i in range(0 , 100):
    os.rename(f"data/Day{i +1}" , f"data/Tutorials {i +1}" )
    
"""


#--------------------> Deleted 500 directories above created.<---------------------#

"""
for i in range(0 , 50):
    os.rmdir(f"data/Tutorials {i +1}")

"""

#Create files inside the directories

"""
for i in range(51 , 100):
    os.mkdir(f"data/Tutorials {i + 1}/ram")
    print("Director created now")

"""

#---------> get current working directories using python os module.<-----------------#
"""

pwd=os.getcwd()
print(pwd)

"""
#----------> Change current working directory <---------------#

"""
pwd=os.getcwd()
print(pwd)
"""

#------------> Change current working directory <---------------#

"""
os.chdir("/home/rampanwar/learning/python-learning")
pwd=os.getcwd()
print(pwd)

"""

#--------------> List directories <--------------------#

"""for i in range(0, 100):
    os.mkdir(f"data/Tutorials {i +1}")
    print("Directories are created now")
    


dir=os.listdir("data")

for file in dir:
    print(file)

"""