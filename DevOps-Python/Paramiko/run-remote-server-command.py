#!/usr/bin/python
import paramiko
import os
from getpass import getpass

# Enter Server details
hostname=os.environ.get('HOSTNAME')
port = '22'
username=input("Enter server username:- ")
password=getpass("Please enter server password:- ")

# Enter command you want to exec on server
command = input("Which command you want to exec:- ")


with paramiko.SSHClient() as client:
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())  #used to no host verification

    client.load_system_host_keys()
    client.connect(hostname, port, username, password)

    (stdin, stdout, stderr) = client.exec_command(command)

    output = stdout.read()
    print(str(output, 'utf8'))