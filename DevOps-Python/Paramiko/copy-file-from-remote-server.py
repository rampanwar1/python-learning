#!/usr/bin/python

import paramiko
import os
import getpass

from getpass import getpass

hostname = os.environ.get('HOSTNAME')
port = os.environ.get('PORT')
username = input("Enter username:- ")
password = getpass()   # This module is used to hide input password in promt

remote_path = f'/home/{username}/test'
output_file = 'data.text'

with paramiko.SSHClient() as client:
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.load_system_host_keys()
    client.connect(hostname, port, username, password)

    sftp_client = client.open_sftp()
    sftp_client.get(remote_path, output_file)
    print("file downloaded")