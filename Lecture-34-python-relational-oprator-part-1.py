"""
Python relational operator used to find relation between the values.

Types  of relational values.


<------------>
    A) >   ( grater then )
<------------>

>>> 1000>2000
False
>>> 10>8
True
>>> 10>10
False

# operator for string.

>>> 'ram'<'shyam'
True
>>> ord('r')
114
>>> ord('s')
115

>>> 'india'<'us'
True

#python used unicode number to compare   string value. to get unicode number use ord function Like bellow

>>> ord('a')
97
>>> ord('b')
98


<------------>
B) < ( Less then )
<------------>

>>> 10<19
True
>>> 10<9
False
>>> 10<10
False

<------------>
C) >= ( grater then equal to)
<------------>

>>> 10>=12
False
>>> 10>=10
True
>>> 10>=8
True

<------------>
D) <= ( less then equal to)
<------------>

>>> 10<=10
True
>>> 10<=12
True
>>> 10<=8
False


"""

age=input("Please enter your age")

if age>='25':
    print("You are elder of shyam")
else:
    print("you are younger brother of shyam")
    