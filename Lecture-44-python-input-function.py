"""
Python input fucntion value read from terminal when program run.

"""

"""

#-----------------> Example:- 1 <-----------------#

name=input("Please enter your name")

print(name)

"""

#-------------> Example:- 2 <-------------------------#

"""
a=input("enter value for a:- ")
b=input("enter value for b:- ")
print(a+b)
print(type(a))
print(type(b))
print(type(a+b))

"""

#output:- 1212

#Note:- when we provide any types of  input values it will take as str.

#to solve this problem we can use typecasting 

#-------------> Example:- 3 <------------------#

"""
a=int(input("enter value for a:- "))  #it's called type casting
b=int(input("enter value for b:- "))
print(a+b)
print(type(a))
print(type(b))
print(type(a+b))
"""

#Note:- We need to enter manual value type Like, int,float,boot etc,

# we can you eval function this is auto read python value.

a=eval(input("Enter value for a;- "))
b=eval(input("enter value for b:- "))
print(a+b)
print(type(a))
print(type(b))

#Eval function automatically read actual value.









