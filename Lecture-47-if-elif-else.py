varname=eval(input("enter data type:- "))

if (type(varname)  == int):
    print("This data type is Integer")
elif (type(varname) == str):
    print("this datatype is string")
elif (type(varname) == float):
    print("This datatype  is float ")
elif (type(varname) == complex):
    print("This datatype is complex")
elif (type(varname) == bool):
    print("This datatype is bool")
elif (type(varname) == list):
    print("This datatype is list")
elif (type(varname) == tuple):
    print("This datatype is tuple")
elif (type(varname) == set):
    print("This datatype is set")
elif (type(varname) == frozenset):
    print("This datatype is frozenset")
elif (type(varname) == dict):
    print("This datatype is dict")
elif (type(varname) == range):
    print("This datatype is range")
elif (type(varname) == bytes):
    print("This datatype is bytes")
elif (type(varname) == bytearray ):
    print("This data type is bytearray")
elif (type(varname) == None):
    print("This datatype is none")
else:
    print("unknown datatype")
