1. Run Simple commannd

pyhone3 test.py

2. get bytecode of python project.

command:- python3 -m py_compile test.py

3. get .exe file

Note:- TO get .exe file of your python code first you need to install pyinstaller.

Command:- pip install pyinstaller

Create .exe command

Command:- pyinstaller test.py

Types of functions.

1. Inbuild functions  

Buit in fuctions created by python comunity

2. Module level functions
 Group of functions called modules leve functions Eg:- Boto3

3. userdefine funtions

User define created by user.


Call calendar module fucntion.

import calendar

calendar.month(2021,10)

print calendar.month(2021,10)


          Lecture:- 6  Python data type.


int , float  , complex , bool , str ,list  , tuple , set , frozenset  , dict , range , bytes , bytearry , none



               Lecture:- 7 (integer)

A) int (integer)

types of integer 


    Type          short_name      value                                    values_example
  
a)   Decimal      base10          (0-9)                                     1997

b)   binary       base2           (0-1)                                     11111001101

c)   octal        base8           (0-7)                                     3715

d)   Hexadecimal  base16          (0-9,AF A=10,B=11,C=12,D=13,E=14,F=15)    7CD

How get use values in variable in Decimal,binary,octal,Hexadecimal

Decimal--->
a=1997
print(a)
output:- 1997

binary--->
a=0b11111001101 or 0B11111001101
print(a)
output:- 1997

Octal--->
a=0O3715  or a=0o3715
print(a)
output:- 1997

Hexadecimal--->
a=0X7CD
print(a)
output:- 1997

IMP:- how to get variable data size

command:-  import sys
           a=1997
           sys.getsizeof(variable_name)  
           Ex:- sys.getsizeof(a)  
           output:- 24 Byte

        Lecture:- 8 Float values

>>> a=177.4
>>> print(a)
177.4
>>> type (a)
<type 'float'>

I we wanted to add 1 lakh values  using variable we can define like bellow using float variable.

Example:- 1

>>> a=1.e5
>>> print(a)
100000.0
>>> type(a)
<type 'float'>

Explain:- e=10
          5 means 5 time 10
        10x10x10x10x10 = 100000 x 1 = 100000

Example:- 2

>>> a=2.e5
>>> print(a)
200000.0
>>> type(a)
<type 'float'>

Explain:- e=10
          5 means 5 time 10
          10x10x10x10x10 = 100000 x 2 = 200000

          <>==================================================>

                   Lecture:- 10 boolen_value
what is boolen values:- boolet value provide value in true and false.

Example:-

        1.  >>> a=100
            >>> b=120
            >>> ab=a>b
            >>> print(ab)
            output:- False

        2.  >>> a=100
            >>> b=200
            >>> ab=a>b
            >>> print
            >>> print(ab)

            output:- True  =  1
                     False =  0

<=============Lecture:- 22 Python-Base-conversion=======================>

desimal value convert in binary , octal and hexa desimal using python fuction

1. Desimal to binary

>>> bin(1997)
'0b11111001101'

2. desimal to octal

>>> oct(1997)
'0o3715'

3. Hex to desimal

>>> hex(1997)
'0x7cd'
>>> 

<==============Lecture:- 23 ( convert data type in each other )==========>

Imp:- we can convert 5 data type in each other.

a) int()
b) flot()
c) bool()
d  complex()
e  str()

1. convert datatype in integer 

a) float to int

Ex:- >>> int(55.78)
     55
     >>> a=int(55.78)
     >>> print(a)
     55
     >>> type(a)
     <class 'int'>
     >>> 

b) convert bool to int 

     >>> int(False)
     0
     >>> int(True)
     1
     >>> a=int(True)
     >>> print(a)
     1
     >>> type(a)
     <class 'int'>
     >>> 

c) we cant convert datatype from complex to int.

d) convert str to int.

   >>> int(1997)
   1997
   >>> a=int(1997)
   >>> print(a)
   1997
   >>> type(a)
   <class 'int'>
   >>> 

Note:- we can convert str to int bt only we can convert only decimal type number covert in int. not float, binary and hexadesimal.

<================Lecture:- 24 (convert data in float) ==================>

<----- int to float ------>

>>> float(1997)  ------> (decimal)
1997.0

>>> float(0b101001) ----->(binary)
41.0

>>> float(0o3715) -----> (octal)
1997.0
>>> 

>>> float(0x7cd) ------> Hexadecimal
1997.0
>>> 

<---- bool to float ----->

>>> float(True)
1.0
>>> float(False)
0.0
>>> 

<---Complex to float---->

can't convert complex to float

<------str to float------->

>>> float(1997)
1997.0
>>> 

we can convert int decimal value type in float. like int.

<================Lecture:- 24 (convert data in bool) ==================>

int to bool

>>> bool(0000)
False
>>> bool(1)
True
>>> 

float to bool

>>> bool(22.98)
True
>>> bool(00.000)
False
>>> 

complex to boo

>>> bool(20+23j)
True
>>> bool(00.00)
False
>>> 

str to bool 

>>> bool('ram')
True
>>> bool('indore')
True
>>> bool('')
False
>>> 

<================Lecture:- 25 (convert data in complex) ==================>

convert int to complex 

>>> complex(1000)
(1000+0j)

# add number according to you
>>> complex(1000,10)  
(1000+10j)
>>> 


