"""

Membership operator used for find word in variable value.

Types of membership operator

1. in

2. not in 

"""

#-----------> in ( type ) <----------------#
#Note;- a value find in ca value

"""
a=10

c=[1,2,3,4,5,7,8,9,];

if (a in c):
    print("a is available in c")
else:
    print("a is not available in c")
    
"""

#-----------> not in ( type ) <----------------#

"""a=10

c=[1,2,3,4,5,7,8,9];

if (a not in c):
    print("a is not available in c")
else:
    print("a is available in c")
    
"""
    