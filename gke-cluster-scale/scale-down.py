import os
import function as func

if __name__ == "__main__":
    cluster_name = os.getenv("CLUSTER_NAME")
    minimum_node = int(os.getenv("MINIMUM_NODE"))
    max_node = int(os.getenv("MAXIMUM_NODE_SCALING_UP"))
    region_name = os.getenv("REGION_NAME")
    node_pool_names = eval(os.getenv("NODE_POOL_NAME"))
    desired_node  = int(os.getenv("DESIRED_NODE_SCALING_UP"))

    for node_pool in node_pool_names:
        node_pool = node_pool.strip()
        func.scale_gke_node_pool(node_pool, cluster_name, minimum_node, max_node, region_name )


        
if __name__ == "__main__":
    cluster_name = os.getenv("CLUSTER_NAME")
    region_name = os.getenv("REGION_NAME")
    node_pool_names = eval(os.getenv("NODE_POOL_NAME"))
    desired_node  = int(os.getenv("DESIRED_NODE_SCALING_DOWN"))

    for node_pool in node_pool_names:
        node_pool = node_pool.strip()
        func.resize_cluster_nodes(cluster_name, node_pool, desired_node, region_name)