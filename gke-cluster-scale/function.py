import subprocess
from dotenv import load_dotenv
load_dotenv()

def scale_gke_node_pool(node_pool_name, cluster_name, minimum_node, max_node, region_name):
    command = [
        f"gcloud container clusters update {cluster_name} "
        f"--enable-autoscaling --node-pool={node_pool_name} "
        f"--min-nodes={minimum_node} --max-nodes={max_node} "
        f"--region={region_name}"
    ]
    subprocess.run(command, shell=True, check=True)
    print(f"\n ||===> Scaling cluster {cluster_name} to {minimum_node} and {max_node} for node pool: {node_pool_name} <===||")
    


def resize_cluster_nodes(cluster_name, node_pool_name, desired_node, region_name):
    command = [
        f"gcloud container clusters resize {cluster_name} "
        f" --num-nodes={desired_node} --node-pool={node_pool_name} --region={region_name} -q"
    ]
    subprocess.run(command, shell=True, check=True)

    print(f"\n ||===> resize cluster nodes {cluster_name} to desired_node {desired_node} for node pool {node_pool_name} <===||")