

#------------->   1.  Print any word on Dual quots <-------------------#

#Method:-1

print("Welcome to \"india\" this is \"DevOps\" team")

#Method:-2

print('Welcome to "india" this is DevOps team')


# output:-   Welcome to "india" this is "DevOps" team
#            Welcome to "india" this is DevOps team





#------------>  2. Print any word in singal quots <------------------#

print ('This is \'DevOps\' team')

print('''This is 'DevOps' team''')



# output:- This is 'DevOps' team
#          This is 'DevOps' team




#---------->  3. Print Singal  backslash <---------------#


print("This is \ DevOps  team")

#Print Singal backslash  and of line.

print("This is DataOps Team \\")

#Print Dual backslash  and of line.

print("This is DevOps team \\\\")


#Note:- Dual backslash (\\) means  singal (\) backslash
#Note:- Four backslash (\\\\) means dual (\\) backslash



#-----------------> 4. Break singal line in multiple line <---------------------#

#Break singal line in two lines 

print("Welcome to india. \nThis is india")

#Output:- Welcome to india. 
#          This is india

#Break singal line in three lines using \n

print("This is india. \nwelcome to india. \nI am from india")


#output:- #      This is india. 
         #      welcome to india. 
         #      I am from india


#----------------> 5. Add tab space between the line words using \t <-----------------------#

print("123456789")
print("\tWelcome to india")

#Output:- 123456789
#                 Welcome to india

#Note:- default tab space It will take 8 character. we can increase and decrease in required tab character 
print("123456789")
print("\tWelcome to team india.".expandtabs(tabsize=3))

#output:- 123456789
#                 Welcome to team india.

print("123456789")
print("\tThis is team india and welcome to team india".expandtabs(tabsize=6))

#output:-  123456789
#                This is team india and welcome to team india



#----------------> 6. Replace world with using \r <--------------------------#

#Example:- 1

print("welcome to our \rindia")

#output:- indiame to our

#Example:- 2

print("12344567 this is my \rnumber")

#output:- number67 this is my


#----------------> 7. Add backspace in python code using \b  <-------------------#

#Example:- 1

print("Welcome to team india this is our india\b team")

#output:- Welcome to team india this is our indi team

#Example:- 2

print("1997\b This is my bith year")

#output:- 199 This is my bith year

#-------------> 8. Print Octal value in string <---------------------#

print('\110\145\154\154\157')

#output:- Hello


#-------------> 9. Print Hexa-decimal to string --------------->

print('\x48\x65\x6c\x6c\x6f')

#output:- Hello


#Conclution

print("Welcome this india\b \nwelcome to my country india. \tthis is very good \"country\" for visitors. here is lots of \'tourists\' places are available".expandtabs(tabsize=4))

#output:- Welcome this indi 
#     welcome to my country india.    this is very good "country" for visitors. here is lots of 'tourists' places are available

