#-----------> Example= 1 <--------------#
#Simple python function

"""
def myfirstfuntion(name):
    print(name, "is tMDC DevOPs team member and we are all planing to enjoy this weekend")

myfirstfuntion('Ram')
myfirstfuntion('Amit')
myfirstfuntion('Chetan')
myfirstfuntion('Suraj')
myfirstfuntion('Siva')
myfirstfuntion('Ravi')

"""

#-----------> Example= 2 <----------------#

"""
#Python support four types of arguments
1. Default Arguments
2. keyword Arguments
3. Variable length arguments
4. Required Arguments


#Function:- 1
def mysecondfunction(a,b):
    print("The calculation of a and b are=", (a+b)/2)
        
mysecondfunction(10,20)

#Note:- To pass value only for a 


#Function:- 2
def mysecondfunction(a=10,b=20):
    print("The calculation of a and b are=", (a+b)/2)
        
mysecondfunction(b=4)
mysecondfunction(a=4)

#Note we can pass spesfic variable value also.

"""


def mythirdfunc(a=100,b=100):
    if a >=b:
        print("A is greater than b")
    elif a <= b:
        print("a is less than b")
    else:
        print("unknown number")

mythirdfunc(a=100,b=200)


    
        
        



    