#Python provide three types of condition statements if, if-else and if-elif,else.
#Some examples are given bellow to describe if condition


"""
#------------> Example:- 1 <---------------#

a=100
b=20

if a>b:
    print("A is grater then b", a , b,a+b)
"""

"""
#-------------> Example:- 2 <-------------#

a=int(input("Add value for a"))
b=int(input("Add value for b"))

if a+b:
    print("The sum of a and b", a+b)  #The sum of a and b 1211 (Wrong value) 
    print(type(a))
    print(type(b))
"""
ntput("The sum of a and b", a+b)  #The sum of a and b 1211 (Wrong value) 

print(type(a))
"""
#-------------> Example:- 3 <-------------#

a=eval(input("Add value for a"))
b=eval(input("Add value for b"))

if a+b:
    print("The sum of a and b", a+b)
    print(type(a))
    print(type(b))
"""

name=input("Please enter name")

if name in ["ram","Amit","Suraj","Chetan","Subhranil"]:
    print("you are DevOps team member of TMDC")
else:
    print("you are not member of TMDC")