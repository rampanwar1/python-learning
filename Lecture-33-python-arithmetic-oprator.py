""""
Operators are used to perform on variable and values

Python divides operator in following types

1. Arithmetic Operators.

2. Relational Operator

3. Logical operators

4. Assignment operators

5. Identity operators

6. Membership operators


"""


#----------------------> Arithemic_oprator <--------------------#
"""
1. Arithmetic Operators. 

Arithmetic operators are used to perform mathematical operations Like Addition, subtraction , multiplication and division.

Types of Arithmetic Operator


<----------------------->
A) +      ( Operators )
<------------------------->
#Example:- 1

>>> a=10
>>> b=10
>>> a+b
20

#Example:- 2 

>>> 1000+1000
2000

# join string using + operator

>>> 'Ram'+'Shyam'
'RamShyam'

>>> 'ram'+'10'
'ram10'


<------------------------->
B) -   ( Operators )
<------------------------->


Example:- 

>>> 1000-100
900
>>> 9.6-3
6.6

<------------------------->
C) * ( Operators )
<------------------------->

>>> 10*10
100
>>> 100*10
1000
>>> 
>>> 'ram'*10
'ramramramramramramramramramram'

<------------------------->
D) / ( Float Division )
<------------------------->

Note:- It will always send value in float number that why always called it flot division.

>>> 10/4
2.5
>>> 10/2
5.0  #FLoat value
>>> 


<------------------------->
E) % ( Modulo Division )
<------------------------->

>>> 10%4
2
>>> 10%2
0
>>> 

<------------------------->
F) // ( Floor Division )
<------------------------->

>>> 10//4
2
>>> -10//4   #It will provide always positive value.
-3
>>> 10.0//4
2.0
>>> 

<------------------------->
G) ** ( Power )
<------------------------->

>>> 10**4
10000
>>> 10**5
100000
>>>

"""
