# I dentity operator are used to compair object.

"""

Types of identity operator
 
1. is     	  ( Returns true if both variables are the same object )

2. is not    ( Returns true if both variables are not the same object )


"""

#---------> Example of is <--------------#

"""
>>> a=100
>>> b=100
>>> id(a)
9804416
>>> id(b)
9804416

#Another way to find relation using Ideentity operator

>>> a is b 
True

"""

#-----------> Example of is not <-------------#
"""
Example:- 1
>>> a=1997
>>> b=1998
>>> id(a)
140641899557264
>>> id(b)
140641899557104
>>> a is not b
True

Example:- 2

>>> a=10
>>> b=10
>>> id(a)
9801536
>>> id(b)
9801536
>>> a is not b
False


"""