import os
import function as func

if __name__ == '__main__':
    country_name = str(os.getenv("COUNTRY_NAME"))
    city_name = str(os.getenv("CITY_NAME"))
    state_name = str(os.getenv("STATE_NAME"))
    func.function(country_name, city_name, state_name)
