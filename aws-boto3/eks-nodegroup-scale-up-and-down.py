import boto3

def scale_down_eks_node_group(eks_cluster_name, eks_nodegroup_name, desired_capacity):
    session = boto3.Session(profile_name=aws_profile, region_name=aws_region)
    eks_client = session.client('eks')

    # Get the current node group configuration
    response = eks_client.describe_nodegroup(
        clusterName=eks_cluster_name,
        nodegroupName=eks_nodegroup_name
    )

    # Update the node group with the new desired capacity
    response = eks_client.update_nodegroup_config(
        clusterName=eks_cluster_name,
        nodegroupName=eks_nodegroup_name,
        scalingConfig={
            'desiredSize': desired_capacity
        }
    )

    print(f"Scaling down {eks_nodegroup_name} in {eks_cluster_name} to {desired_capacity} instances.")

if __name__ == "__main__":
    eks_cluster_name = 'dev-test-cluster'
    eks_nodegroup_name = 'dev-test-bode'
    desired_capacity = 4
    aws_profile = 'rubik-engineer'
    aws_region = 'ap-northeast-3'

    scale_down_eks_node_group(eks_cluster_name, eks_nodegroup_name, desired_capacity)
