import boto3

def node_scale_up_down(cluster_name, eks_nodegroup_name, desired_capacity):
    session = boto3.Session(profile_name=aws_profile, region_name=aws_region)
    eks_client =  session.client('eks')
    
    response = eks_client.describe_nodegroup(
        clusterName=eks_cluster_name,
        nodegroupName=eks_nodegroup_name
    )
    
    response = eks_client.update_nodegroup_config(
        clusterName=eks_cluster_name,
        nodegroupName=eks_nodegroup_name,
        scalingConfig={
            'desiredCapacity': desired_capacity
        }
    )
    
    print(f"Sacling down {eks_nodegroup_name} in {eks_cluster_name} to {desired_capacity} instances.")

if '__name___' == "__main__":
    eks_cluster_name = 'dev-test-cluster'
    eks_nodegroup_name = 'dev-test-nodegroup'
    desired_capacity = 2
    aws_profile = 'rubik-engineer'
    aws_region = 'ap-northeast-3'

    node_scale_up_down(eks_cluster_name, eks_nodegroup_name, desired_capacity)