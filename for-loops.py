
"""
Defination:-  for loop is used for iterating over a sequence (that is either a list, a tuple, a dictionary, a set, or a string).

#------------> Example:- 1 <----------------#

name=str(input("please enter input for i"))

for i in name:
    print(1)

"""

#---------------> Example:- 2 <----------------#


# For loops with break statements

"""
devopsteam=["Ram","Amit","Suraj","cheta","Subranil","siva","Ravi"]

for i in devopsteam:
    if i == 'Suraj':
        break
    print(i, "he is out DevOps team member")

#--> Note:- Break statements  will break line according to provide condition
 
"""

#--------------> Example:- 3 <-----------------#

#for loops with range function (break statements)

"""
for i in range(20):
    if i == 15:
        break
    print(i + 1,"This is test value")

"""   

#----------------> Example:- 4 <-----------------#


""
for i in range(1 , 10):
    if i == 15:
        break
    print(i)
 
""


#---------------> Example:- 5 <------------------#

"""

devopsteam=["Ram","Amit","Suraj","Chetan","Siva","Ravi"]

for name in devopsteam:
    if name == 'Siva':
        continue
    else:
        print(name )
"""


    


