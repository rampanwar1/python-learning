"""
1. What is nested if ?

If you can have if statement in if statement , this called nested if statement.

"""

"""
#------------> example:- 1 <-------------#


a=10
b=20
c=30

if a==10:
    if b==20:
        if c==30:
            print("value of a = ", a)
            print("value of b =", b) 
            print("value of c=" , c)
"""

#------------> example:- 2 <-------------#


a=10
b=20
c=30

if a==10:
    if b==20:
        if c==30:
            print("value of a = ", a)
            print("value of b =", b) 
            print("value of c=" , c)
        else:
            print("vaule for c invalid=", c)
    else:
        print("value for b invaild=", b)
else:
    print("value for a invalid", a)
