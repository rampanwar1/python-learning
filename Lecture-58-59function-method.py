#------> Simple function

def myfunction():
    print("Welcome to India")

myfunction()


#------> Function with arguments 

def function2(name):
    print("Goog morning", name)

function2("Ram")
function2("Shyam")
function2("Indore")

#-----> Function with multiple args

def funcion3(a, b):
    z=a+b
    print("sum is", z)

a=int(input("Enter first number:- "))
b=int(input("Enter second value:- "))

funcion3(a, b) #------> Calling function


#-----> Function with return values

def function3(a, b):
    z=a+b
    return z
a=int(input("Enter your first value:- "))
b=int(input("Enter your second value:- "))

print("sume is", function3(a, b))

function3(a, b) #------> Calling function

#-----> Function with conditions

def function4(a, b):
    if a > b:
        print("a is Greater then b")
    else:
        print("a is not greater then b")

a=int(input("Enter enter value for a :-" ))
b=int(input("Enter value for b :- "))

function4(a, b) #-----> Calling function