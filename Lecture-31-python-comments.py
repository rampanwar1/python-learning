#-----------> How to comment line in python <------------#

print("hellow world")


#--------------> comment multiple line <-------------------#

""""
This is comment line
using dual quots

"""

print("Welcome to india")



#-------------> Comment multiple line using singal quots <--------------#

''''
This is comment line 
using the singal quots

'''

print("singal quots comment")
