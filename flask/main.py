
from flask import Flask , render_template
app = Flask(__name__)

@app.route('/')
def hello_ram():
    return render_template ('index.html')


@app.route('/about')
def about():
    name = "Ram Panwar!"
    return render_template ('about.html', enter_name= name)

if __name__ == '__main__':
    app.run(debug=True, port=8085, host="0.0.0.0" )